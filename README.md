Autor: Nicolás Castilla
Fecha: 22/05/2018

Arquitectura propuesta:
La arquitectura porpuesta es Multilayered architecture donde es basado en cliente - servidor y almacenamiento (persistencia).
Dado el pryecto y tiempos de desarrollo se eligio esta arquitectura pora modelar y desarrollar rapidamente sin la nesecidad de complejos y pesados frameworks. 

Compatibiliodad:
Esta desarrollado para soportar desde Android 4.0 +

Instrucciones: 
Descargar el archivo desdes el correo adjunto o desde https://bitbucket.org/nic_cast/zemogatestnicolas/src/master/apk/app-debug.apk y correr la aplicación.
Si no permite ir a setting -> seguridad -> habilitar "Origenes desconocidos"
en el link https://drive.google.com/file/d/1UM5K6PdmqzSvxXtqFBzXObZ-4tYz97iZ/view?usp=sharing pueden encontrar el codigo fuente y en la carpeta apk encuentran el archivo instalador,
que es solo correrlo.
Para compilarlo se requiere clonar el proyecto desde repositorio o descomprimiendo desde el rar del link, abrir android studio y abrir el proyecto, luego es solo Run app.


3Parte libs:
- Se uso una libreria de SwipeMenuListView para la opción de swipe para poder borrar un post ya que 
Android no tiene nativamente esa funcionalidad y es demorado implementarla desde 0.
- JsonOrg Esta libreria es para poder crear un objeto tipo Json o JsonArray y poder manipular los datos que llegan desde el servidor.

Imagenes:
Imagenes descargadas desde https://icons8.com/
y editadas con Adobe Firewoks.
