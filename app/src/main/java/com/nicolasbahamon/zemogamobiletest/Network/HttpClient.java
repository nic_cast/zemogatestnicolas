package com.nicolasbahamon.zemogamobiletest.Network;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import com.nicolasbahamon.zemogamobiletest.BuildConfig;
import org.json.JSONObject;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Locale;

import javax.net.ssl.HttpsURLConnection;

import static android.content.Context.MODE_PRIVATE;


/**
 * Created by Nicolas Bahamon on 10/14/16.
 * Way to connect to web server HttpURLConnection no deprecated
 */

public class HttpClient {

    private class FileProperties{
        public String uri;
        public String idActivity;

        public FileProperties(){}
    }

    /**
     * Constant definitions
     */
    private static final String APIURL = "---";


    //services

    public int responseCode = 0;


    //variables
    public Context mContext;
//******************************** methods ****************************************

    public HttpClient(Context context){

        mContext = context;
    }

    /**
     * Transforma el imputstream en string para ser procesado
     * @param inputStream imputstream con respuesta servidor
     * @return string
     * @throws IOException
     */
    private static String convertInputStreamToString(InputStream inputStream) throws IOException{
        String result = "";
        if(inputStream != null) {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line;

            while ((line = bufferedReader.readLine()) != null)
                result += line;

            inputStream.close();
        }
        return result;

    }

    /**
     * Custom Post methot to request info from a url and return a String to be procesed
     * @param UrlHttp Url del web services a procesar
     * @param objects json con los objetos a enviar al servidor
     * @param fileUpload
     * @return string con lo descargado del servidor
     */
    private String HttpConnectPut(String UrlHttp, JSONObject objects, FileProperties fileUpload){
        InputStream in;
        int resCode;

        try {
            URL url = new URL(UrlHttp);
            URLConnection urlConn = url.openConnection();
            System.out.println("URL: "+UrlHttp);
            if (!(urlConn instanceof HttpURLConnection)) {
                throw new IOException("URL is not an Http URL");
            }
            HttpsURLConnection httpConn = (HttpsURLConnection) url.openConnection();
            httpConn.setDoOutput (true);
            String version = BuildConfig.VERSION_NAME;
            String androidOS = Build.VERSION.RELEASE;
            String language = Locale.getDefault().getLanguage();
            String manufacturer = Build.MANUFACTURER;
            String model = Build.MODEL;
            String agent = "CustomData/"+version+"(Android; "+androidOS+": "+manufacturer+"- "+model+")";
            httpConn.setRequestProperty("User-Agent",agent);
            httpConn.setRequestProperty("Accept", "application/json");
            httpConn.setRequestProperty("Accept-Language", language);
            httpConn.setRequestProperty(
                    "Content-Type", "multipart/form-data;boundary=*****");

            httpConn.setInstanceFollowRedirects(false);
            httpConn.setRequestMethod("PUT");
            try {

                if(objects != null && fileUpload == null) {
                    OutputStream out = new BufferedOutputStream(httpConn.getOutputStream());
                    out.write(objects.toString().getBytes("UTF-8"));
                    out.close();
                }
                else if(fileUpload != null) {

                    String attachmentName = "photo_company";
                    String attachmentFileName = "bitmap.jpg";
                    String crlf = "\r\n";
                    String twoHyphens = "--";
                    String boundary =  "*****";

                    InputStream iStream =   mContext.getContentResolver().openInputStream(Uri.parse(fileUpload.uri));
                    byte[] inputData = getBytes(iStream);

                    DataOutputStream request = new DataOutputStream(httpConn.getOutputStream());
                    request.writeBytes(twoHyphens + boundary + crlf);
                    request.writeBytes("Content-Disposition: form-data; name=\"" +
                            attachmentName + "\";filename=\"" +
                            attachmentFileName + "\"" + crlf);
                    request.writeBytes(crlf);
                    request.write(inputData);
                    request.writeBytes(crlf);
                    request.writeBytes(twoHyphens + boundary + twoHyphens + crlf);
                    request.flush();
                    request.close();



                }

                resCode = httpConn.getResponseCode();
                System.out.println("ResponseCode: "+UrlHttp+" = "+resCode);


                if (resCode == HttpURLConnection.HTTP_OK) {
                    in = httpConn.getInputStream();
                    String response = convertInputStreamToString(in);
                    System.out.println("Response from: "+UrlHttp+"= "+response);
                    return response;
                }
                else{
                    InputStream error = httpConn.getErrorStream();
                    String response = convertInputStreamToString(error);
                    System.out.println("Response Error from: "+UrlHttp+"= "+response);
                    return response;
                }


            } finally {
                httpConn.disconnect();
            }

        }

        catch (MalformedURLException e) {
            Log.e("MalformedURL",e.toString());
            e.printStackTrace();
        }

        catch (IOException e) {
            Log.e("IOException",e.toString());
            e.printStackTrace();
        }

        return null;
    }


    /**
     * Custom Post methot to request info from a url and return a String to be procesed
     * @param fileUpload if upload image some extra data
     * @param UrlHttp Url del web services a procesar
     * @param objects json con los objetos a enviar al servidor
     * @return string con lo descargado del servidor
     */
    public String HttpsConnectPost(String UrlHttp, JSONObject objects, FileProperties fileUpload){
        InputStream in;
        int resCode;

        try {
            URL url = new URL(UrlHttp);
            URLConnection urlConn = url.openConnection();
            System.out.println("URL: "+UrlHttp);
            if (!(urlConn instanceof HttpURLConnection)) {
                throw new IOException("URL is not an Http URL");
            }
            HttpsURLConnection httpConn = (HttpsURLConnection) url.openConnection();
            httpConn.setDoOutput (true);
            String version = BuildConfig.VERSION_NAME;
            String androidOS = Build.VERSION.RELEASE;
            String language = Locale.getDefault().getLanguage();
            String manufacturer = Build.MANUFACTURER;
            String model = Build.MODEL;
            httpConn.setRequestProperty("Content-Type","application/json");
            httpConn.setRequestProperty("Accept", "application/json");
            httpConn.setRequestProperty("Accept-Language", language);
                httpConn.setInstanceFollowRedirects(false);
            httpConn.setRequestMethod("POST");
            try {

                if(objects != null && fileUpload == null) {
                    OutputStream out = new BufferedOutputStream(httpConn.getOutputStream());
                    out.write(objects.toString().getBytes("UTF-8"));
                    out.close();
                }
                else if(fileUpload != null) {

                    String attachmentName = "photo_company";
                    String attachmentFileName = "bitmap.jpg";
                    String crlf = "\r\n";
                    String twoHyphens = "--";
                    String boundary =  "*****";

                    InputStream iStream =   mContext.getContentResolver().openInputStream(Uri.parse(fileUpload.uri));
                    byte[] inputData = getBytes(iStream);

                    DataOutputStream request = new DataOutputStream(httpConn.getOutputStream());
                    request.writeBytes(twoHyphens + boundary + crlf);
                    request.writeBytes("Content-Disposition: form-data; name=\"" +
                            attachmentName + "\";filename=\"" +
                            attachmentFileName + "\"" + crlf);
                    request.writeBytes(crlf);
                    request.write(inputData);
                    request.writeBytes(crlf);
                    request.writeBytes(twoHyphens + boundary + twoHyphens + crlf);
                    request.flush();
                    request.close();



                }

                resCode = httpConn.getResponseCode();
                System.out.println("ResponseCode: "+UrlHttp+"= "+resCode);

                responseCode = resCode;
                if (resCode == HttpURLConnection.HTTP_OK) {

                    in = httpConn.getInputStream();
                    String response = convertInputStreamToString(in);
                    System.out.println("Response from: "+UrlHttp+"= "+response);
                    return response;
                }
                else{
                    InputStream error = httpConn.getErrorStream();
                    String response = convertInputStreamToString(error);
                    System.out.println("Response Error from: "+UrlHttp+"= "+response);
                    return response;
                }


            } finally {
                httpConn.disconnect();
            }

        }

        catch (MalformedURLException e) {
            Log.e("MalformedURL",e.toString());
            e.printStackTrace();
        }

        catch (IOException e) {
            Log.e("IOException",e.toString());
            e.printStackTrace();
        }

        return null;
    }
    /**
     * Custom Post methot to request info from a url and return a String to be procesed
     * @param fileUpload if upload image some extra data
     * @param UrlHttp Url del web services a procesar
     * @param objects json con los objetos a enviar al servidor
     * @return string con lo descargado del servidor
     */
    public String HttpConnectPost(String UrlHttp, JSONObject objects, FileProperties fileUpload){
        InputStream in;
        int resCode;

        try {
            URL url = new URL(UrlHttp);
            URLConnection urlConn = url.openConnection();
            System.out.println("URL: "+UrlHttp);
            if (!(urlConn instanceof HttpURLConnection)) {
                throw new IOException("URL is not an Http URL");
            }
            HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
            httpConn.setDoOutput (true);
            String version = BuildConfig.VERSION_NAME;
            String androidOS = Build.VERSION.RELEASE;
            String language = Locale.getDefault().getLanguage();
            String manufacturer = Build.MANUFACTURER;
            String model = Build.MODEL;
            httpConn.setRequestProperty("Content-Type","application/json");
            httpConn.setRequestProperty("Accept", "application/json");
            httpConn.setRequestProperty("Accept-Language", language);
             httpConn.setInstanceFollowRedirects(false);
            httpConn.setRequestMethod("POST");
            try {

                if(objects != null && fileUpload == null) {
                    OutputStream out = new BufferedOutputStream(httpConn.getOutputStream());
                    out.write(objects.toString().getBytes("UTF-8"));
                    out.close();
                }
                else if(fileUpload != null) {

                    String attachmentName = "photo_company";
                    String attachmentFileName = "bitmap.jpg";
                    String crlf = "\r\n";
                    String twoHyphens = "--";
                    String boundary =  "*****";

                    InputStream iStream =   mContext.getContentResolver().openInputStream(Uri.parse(fileUpload.uri));
                    byte[] inputData = getBytes(iStream);

                    DataOutputStream request = new DataOutputStream(httpConn.getOutputStream());
                    request.writeBytes(twoHyphens + boundary + crlf);
                    request.writeBytes("Content-Disposition: form-data; name=\"" +
                            attachmentName + "\";filename=\"" +
                            attachmentFileName + "\"" + crlf);
                    request.writeBytes(crlf);
                    request.write(inputData);
                    request.writeBytes(crlf);
                    request.writeBytes(twoHyphens + boundary + twoHyphens + crlf);
                    request.flush();
                    request.close();



                }

                resCode = httpConn.getResponseCode();
                System.out.println("ResponseCode: "+UrlHttp+"= "+resCode);

                responseCode = resCode;
                if (resCode == HttpURLConnection.HTTP_OK) {

                    in = httpConn.getInputStream();
                    String response = convertInputStreamToString(in);
                    System.out.println("Response from: "+UrlHttp+"= "+response);
                    return response;
                }
                else{
                    InputStream error = httpConn.getErrorStream();
                    String response = convertInputStreamToString(error);
                    System.out.println("Response Error from: "+UrlHttp+"= "+response);
                    return response;
                }


            } finally {
                httpConn.disconnect();
            }

        }

        catch (MalformedURLException e) {
            Log.e("MalformedURL",e.toString());
            e.printStackTrace();
        }

        catch (IOException e) {
            Log.e("IOException",e.toString());
            e.printStackTrace();
        }

        return null;
    }

    public byte[] getBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];

        int len = 0;
        while ((len = inputStream.read(buffer)) != -1) {
            byteBuffer.write(buffer, 0, len);
        }
        return byteBuffer.toByteArray();
    }

    /**
     * Custom Get methot to request info from a url and return a String to be procesed
     * @param UrlHttp Url for the web Service
     * @param objects Json to send to the web Service
     * @return string from server
     */
    public String HttpsConnectGet(String UrlHttp, JSONObject objects){

        URL url;

        int resCode;
        InputStream in;
        try {
            url = new URL(UrlHttp);
            HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
            String version = BuildConfig.VERSION_NAME;
            String androidOS = Build.VERSION.RELEASE;
            String language = Locale.getDefault().getLanguage();
            String manufacturer = Build.MANUFACTURER;
            String model = Build.MODEL;
            urlConnection.setRequestProperty("Content-Type","application/json");
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setRequestProperty("Accept-Language", language);
            urlConnection.setInstanceFollowRedirects(false);
            urlConnection.setRequestMethod("GET");
            try {

                resCode = urlConnection.getResponseCode();
                System.out.println("ResponseCode: "+UrlHttp+"= "+resCode);

                responseCode = resCode;
                if (resCode == HttpURLConnection.HTTP_OK) {

                    in = urlConnection.getInputStream();
                    String response = convertInputStreamToString(in);
                    System.out.println("Response from: "+UrlHttp+"= "+response);
                    return response;
                }
                else{
                    InputStream error = urlConnection.getErrorStream();
                    String response = convertInputStreamToString(error);
                    System.out.println("Response Error from: "+UrlHttp+"= "+response);
                    return response;
                }

            } finally {
                urlConnection.disconnect();
            }
        } catch (MalformedURLException e) {
            Log.e("MalformedURL",e.toString());
            e.printStackTrace();
        }

        catch (IOException e) {
            Log.e("IOException",e.toString());
            e.printStackTrace();
        }


        return null;
    }
    public String HttpConnectGet(String UrlHttp, JSONObject objects){

        URL url;

        int resCode;
        InputStream in;
        try {
            url = new URL(UrlHttp);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            String version = BuildConfig.VERSION_NAME;
            String androidOS = Build.VERSION.RELEASE;
            String language = Locale.getDefault().getLanguage();
            String manufacturer = Build.MANUFACTURER;
            String model = Build.MODEL;
            urlConnection.setRequestProperty("Content-Type","application/json");

            urlConnection.setInstanceFollowRedirects(true);
            urlConnection.setRequestMethod("GET");
            try {

                resCode = urlConnection.getResponseCode();
                System.out.println("ResponseCode: "+UrlHttp+"= "+resCode);

                responseCode = resCode;
                if (resCode == HttpURLConnection.HTTP_OK) {

                    in = urlConnection.getInputStream();
                    String response = convertInputStreamToString(in);
                    System.out.println("Response from: "+UrlHttp+"= "+response);
                    return response;
                }
                else{
                    InputStream error = urlConnection.getErrorStream();
                    String response = convertInputStreamToString(error);
                    System.out.println("Response Error from: "+UrlHttp+"= "+response);
                    return response;
                }

            } finally {
                urlConnection.disconnect();
            }
        } catch (MalformedURLException e) {
            Log.e("MalformedURL",e.toString());
            e.printStackTrace();
        }

        catch (IOException e) {
            Log.e("IOException",e.toString());
            e.printStackTrace();
        }


        return null;
    }

}
