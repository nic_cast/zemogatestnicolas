package com.nicolasbahamon.zemogamobiletest;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.nicolasbahamon.zemogamobiletest.Activities.ShowPostInfoActivity;
import com.nicolasbahamon.zemogamobiletest.Models.Post;
import com.nicolasbahamon.zemogamobiletest.Network.HttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends Activity {

    private RelativeLayout filterAllBtn, filterFavBtn;
    private FrameLayout lineA, lineB;
    private ArrayList<Post> listPosts;
    private SwipeMenuListView listView;
    private PostsAdapter adapter;
    private HttpClient httpClient;
    private ProgressBar loading;
    private Button relloadAllBtn, eraseAllBtn;
    private int filterselected = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        filterAllBtn = (RelativeLayout)findViewById(R.id.filterAll);
        filterFavBtn = (RelativeLayout)findViewById(R.id.filterFavorite);
        lineA = (FrameLayout) findViewById(R.id.lineA);
        lineB = (FrameLayout) findViewById(R.id.lineB);
        listView = (SwipeMenuListView) findViewById(R.id.listView);
        httpClient = new HttpClient(getApplicationContext());
        loading = (ProgressBar)findViewById(R.id.progressBar);
        relloadAllBtn = (Button)findViewById(R.id.buttonRl);
        eraseAllBtn = (Button)findViewById(R.id.buttonDelete);

        variablesUses();

        new downloadAllPosts().execute();

    }

    @Override
    protected void onResume() {
        super.onResume();
        // Loading from cache
        if(filterselected == 0)
            loadListPost();
        else
            loadListPostFav();
    }

    private void variablesUses() {
        filterAllBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lineA.setVisibility(View.VISIBLE);
                lineB.setVisibility(View.GONE);
                filterselected = 0;
                loadListPost();
            }
        });
        filterFavBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lineA.setVisibility(View.GONE);
                lineB.setVisibility(View.VISIBLE);
                filterselected = 1;
                loadListPostFav();
            }
        });
        relloadAllBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new downloadAllPosts().execute();
            }
        });
        eraseAllBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Aplicacion)getApplication()).getDB().deleteAll();
                loadListPost();
            }
        });

        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth(((Aplicacion)getApplication()).dp2px(90));
                // set a icon
                deleteItem.setIcon(R.mipmap.ic_delete);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };

// set creator
        listView.setMenuCreator(creator);

        listView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        Log.e("click","delete");
                        deleteByid(position);
                        break;

                }
                // false : close the menu; true : not close the menu
                return false;
            }
        });

        // Left
        listView.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ContentValues values = new ContentValues();
                values.put("id_post",listPosts.get(i).idPost);
                values.put("is_new_post",0);
                ((Aplicacion)getApplication()).getDB().updatePostInfo(values);
                listPosts.get(i).newPost = false;
                adapter.notifyDataSetChanged();

                ((Aplicacion)getApplication()).postInfo = listPosts.get(i);
                startActivity(new Intent(getApplicationContext(), ShowPostInfoActivity.class));
            }
        });
    }

    private void deleteByid(int position) {
        ContentValues values = new ContentValues();
        values.put("id_post",listPosts.get(position).idPost);
        values.put("deleted",1);
        ((Aplicacion)getApplication()).getDB().updatePostInfo(values);
        listPosts.remove(position);
        adapter.notifyDataSetChanged();
    }

    private void loadListPostFav() {
        listPosts = ((Aplicacion)getApplication()).getDB().getAllPostFav();
        adapter = new PostsAdapter(getApplicationContext());
        listView.setAdapter(adapter);
    }

    private void loadListPost(){
        listPosts = ((Aplicacion)getApplication()).getDB().getAllPost();
        adapter = new PostsAdapter(getApplicationContext());
        listView.setAdapter(adapter);
    }

    //****************************** asynctask conectar servidor************************

    /**
     * Call Async the api server and load all info
     */
    private class downloadAllPosts extends AsyncTask<Integer, Void, Integer> {

        private String resp;



        @Override
        protected void onPreExecute(){
            relloadAllBtn.setVisibility(View.GONE);
             loading.setVisibility(View.VISIBLE);
        }


        @Override
        protected Integer doInBackground(Integer... params) {


            resp = httpClient.HttpConnectGet("https://jsonplaceholder.typicode.com/posts",null);

            return null;
        }

        @Override
        protected void onPostExecute(Integer res) {

            if(resp != null){
                if(!resp.equals("")){
                    try {
                        JSONArray jResp = new JSONArray(resp);

                        for(int i =0; i<jResp.length();i++){
                            ContentValues values = new ContentValues();
                            values.put("id_post",jResp.getJSONObject(i).getInt("id"));
                            values.put("body",jResp.getJSONObject(i).getString("body"));
                            values.put("title",jResp.getJSONObject(i).getString("title"));
                            values.put("id_user",jResp.getJSONObject(i).getInt("userId"));

                            ((Aplicacion)getApplication()).getDB().manageInsertion(values);

                        }

                        loadListPost();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            loading.setVisibility(View.GONE);
            relloadAllBtn.setVisibility(View.VISIBLE);
        }
    }



    //************************************ adapter **************************
    public class PostsAdapter extends BaseAdapter {

        private Context MyContext;
        private int page = 2;


        public PostsAdapter(Context context){
            MyContext = context;

        }


        @Override
        public int getCount() {
            return listPosts.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View grid;

            if (convertView == null) {
                grid = new View(MyContext);
                LayoutInflater inflater = (LayoutInflater)MyContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                grid = inflater.inflate(R.layout.row_list_post, parent, false);

            } else {
                grid = (View) convertView;
            }

            TextView name = (TextView)grid.findViewById(R.id.textView4);
            ImageView blueDot = (ImageView)grid.findViewById(R.id.imageViewDot);
            ImageView fav = (ImageView)grid.findViewById(R.id.imageViewFav);

            name.setText(listPosts.get(position).titlePost);
            if(position <= 19 && listPosts.get(position).newPost){
                blueDot.setVisibility(View.VISIBLE);
            }else{
                blueDot.setVisibility(View.GONE);
            }

            if(listPosts.get(position).favorite){
                fav.setVisibility(View.VISIBLE);
            }else{
                fav.setVisibility(View.GONE);
            }




            return grid;
        }
    }


}
