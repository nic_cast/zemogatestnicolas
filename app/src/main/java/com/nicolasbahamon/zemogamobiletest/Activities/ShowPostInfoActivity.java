package com.nicolasbahamon.zemogamobiletest.Activities;

import android.app.Activity;
import android.content.ContentValues;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nicolasbahamon.zemogamobiletest.Aplicacion;
import com.nicolasbahamon.zemogamobiletest.Models.Comments;
import com.nicolasbahamon.zemogamobiletest.Models.Users;
import com.nicolasbahamon.zemogamobiletest.Network.HttpClient;
import com.nicolasbahamon.zemogamobiletest.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ShowPostInfoActivity extends Activity {

    private ImageView favImg;
    private RelativeLayout returnBtn;
    private LinearLayout comensZone;
    private TextView txtName,txtPhone,txtWeb,txtEmail, desciption;
    private HttpClient httpClient;
    private ProgressBar loadinUser, loadinComents;
    private Users user;
    private ArrayList<Comments> commentsList;
    private FrameLayout divLine;
    ViewGroup.LayoutParams params;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_post_info);

        favImg = (ImageView)findViewById(R.id.imageViewFavIn);
        returnBtn = (RelativeLayout)findViewById(R.id.backBtn);
        comensZone = (LinearLayout)findViewById(R.id.comensZone);
        txtName = (TextView)findViewById(R.id.txtName);
        txtPhone = (TextView)findViewById(R.id.txtPhone);
        txtWeb = (TextView)findViewById(R.id.txtWeb);
        txtEmail = (TextView)findViewById(R.id.txtEmail);
        desciption = (TextView)findViewById(R.id.desciption);
        httpClient = new HttpClient(getApplicationContext());
        loadinUser = (ProgressBar)findViewById(R.id.progressBar2);
        loadinComents = (ProgressBar)findViewById(R.id.loadinComents);
        divLine = (FrameLayout)findViewById(R.id.divLine);
        params = divLine.getLayoutParams();

        if(((Aplicacion)getApplication()).postInfo.favorite)
            favImg.setImageResource(R.mipmap.fav_full);
        else
            favImg.setImageResource(R.mipmap.fav_empty);


        favImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!((Aplicacion)getApplication()).postInfo.favorite){
                    ContentValues values = new ContentValues();
                    values.put("id_post",((Aplicacion)getApplication()).postInfo.idPost);
                    values.put("favorite",1);
                    ((Aplicacion)getApplication()).getDB().updatePostInfo(values);
                    favImg.setImageResource(R.mipmap.fav_full);
                }else{
                    ContentValues values = new ContentValues();
                    values.put("id_post",((Aplicacion)getApplication()).postInfo.idPost);
                    values.put("favorite",0);
                    ((Aplicacion)getApplication()).getDB().updatePostInfo(values);
                    favImg.setImageResource(R.mipmap.fav_empty);
                }
            }
        });
        returnBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        desciption.setText(((Aplicacion)getApplication()).postInfo.bodyPost);


        new downloadUser().execute();
        new downloadComents().execute();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ((Aplicacion)getApplication()).postInfo = null;
    }

    //****************************** asynctask conectar servidor************************

    /**
     * Call Async the api server and load all info
     */
    private class downloadUser extends AsyncTask<Integer, Void, Integer> {

        private String resp;



        @Override
        protected void onPreExecute(){

            loadinUser.setVisibility(View.VISIBLE);
        }


        @Override
        protected Integer doInBackground(Integer... params) {


            resp = httpClient.HttpConnectGet("https://jsonplaceholder.typicode.com/users/"+((Aplicacion)getApplication()).postInfo.userId,null);

            return null;
        }

        @Override
        protected void onPostExecute(Integer res) {

            if(resp != null){
                if(!resp.equals("")){
                    try {
                        JSONObject jResp = new JSONObject(resp);

                        ContentValues values = new ContentValues();
                        values.put("id_user",jResp.getInt("id"));
                        values.put("name",jResp.getString("name"));
                        values.put("web",jResp.getString("website"));
                        values.put("email",jResp.getString("email"));
                        values.put("phone",jResp.getString("phone"));

                        ((Aplicacion)getApplication()).getDB().manageInsertionUser(values);




                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            user = ((Aplicacion)getApplication()).getDB().searchUser(((Aplicacion)getApplication()).postInfo.userId);
            loadinUser.setVisibility(View.GONE);
            if(user != null) {
                txtName.setText(user.name);
                txtEmail.setText(user.email);
                txtPhone.setText(user.phone);
                txtWeb.setText(user.website);
            }

        }
    }
    /**
     * Call Async the api server and load all info
     */
    private class downloadComents extends AsyncTask<Integer, Void, Integer> {

        private String resp;



        @Override
        protected void onPreExecute(){

            loadinComents.setVisibility(View.VISIBLE);
        }


        @Override
        protected Integer doInBackground(Integer... params) {

            String url = "https://jsonplaceholder.typicode.com/posts/";
            url +=((Aplicacion)getApplication()).postInfo.idPost;
            url += "/comments";
            resp = httpClient.HttpConnectGet(url,null);

            return null;
        }

        @Override
        protected void onPostExecute(Integer res) {

            if(resp != null){
                if(!resp.equals("")){
                    try {
                        JSONArray jResp = new JSONArray(resp);

                        for(int i =0; i<jResp.length();i++){
                            ContentValues values = new ContentValues();
                            values.put("id_comment",jResp.getJSONObject(i).getInt("id"));
                            values.put("body",jResp.getJSONObject(i).getString("body"));
                            values.put("name",jResp.getJSONObject(i).getString("name"));
                            values.put("email",jResp.getJSONObject(i).getString("email"));
                            values.put("id_post",((Aplicacion)getApplication()).postInfo.idPost);

                            ((Aplicacion)getApplication()).getDB().manageInsertionComments(values);

                        }




                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            commentsList = ((Aplicacion)getApplication()).getDB().getAllCommentsByPost(((Aplicacion)getApplication()).postInfo.idPost);
            loadinComents.setVisibility(View.GONE);

            comensZone.removeAllViews();
            for (Comments com :commentsList) {
                TextView test = new TextView(getApplicationContext());
                test.setText(com.body);
                test.setTextColor(getResources().getColor(R.color.colorNormalText));
                comensZone.addView(test);
                FrameLayout divLineT = new FrameLayout(getApplicationContext());
                divLineT.setBackgroundColor(getResources().getColor(R.color.colorNormalText));
                divLineT.setLayoutParams(params);
                comensZone.addView(divLineT);
            }


        }
    }
}
