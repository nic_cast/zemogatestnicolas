package com.nicolasbahamon.zemogamobiletest.Helpers;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.nicolasbahamon.zemogamobiletest.Models.Comments;
import com.nicolasbahamon.zemogamobiletest.Models.Post;
import com.nicolasbahamon.zemogamobiletest.Models.Users;

import java.util.ArrayList;


/**
 * Created by Nicolas Bahamon on 10/31/16.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    //database version
    private static final int DATABASE_VERSION = 1;
    //database name
    private static final String DATABASE_NAME = "ZemogaTest.db";
    //table names
    private Context context_;

    //creat databases
    private static final String CREATE_TABLE_1 = "CREATE TABLE posts (id_post INTEGER PRIMARY KEY AUTOINCREMENT," +
            "body TEXT," +
            "title TEXT," +
            "favorite INTEGER," +
            "is_new_post INTEGER," +
            "deleted INTEGER," +
            "id_user INTEGER )";
    private static final String CREATE_TABLE_2 = "CREATE TABLE coments (id_comment INTEGER PRIMARY KEY AUTOINCREMENT," +
            "body TEXT," +
            "name TEXT," +
            "email TEXT," +
            "id_post INTEGER )";
    private static final String CREATE_TABLE_3 = "CREATE TABLE users (id_user INTEGER PRIMARY KEY AUTOINCREMENT," +
            "name TEXT," +
            "web TEXT," +
            "email TEXT," +
            "phone TEXT)";



    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        context_ = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_1);
        db.execSQL(CREATE_TABLE_2);
        db.execSQL(CREATE_TABLE_3);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }



    //*********************************** Implementation ************************************

    public void manageInsertion(ContentValues values){

        if(searchPost(values.getAsInteger("id_post")) != null){
            updatePostInfo(values);
        }else{
            insertActivity(values);
        }
    }

    /**
     * Insert values to db
     * @param values
     * @return
     */
    public long insertActivity(ContentValues values){
        SQLiteDatabase db = getWritableDatabase();
        values.put("favorite",0);
        values.put("is_new_post",1);
        values.put("deleted",0);
        long newRowId = db.insert("posts", null, values);
        Log.e("InsertedId",""+newRowId);

        return newRowId;
    }

    public void updatePostInfo(ContentValues values){
        SQLiteDatabase db = getReadableDatabase();

        String selection = "id_post = ?";
        String idPost = values.getAsString("id_post");
        String[] selectionArgs = { idPost };

        int count = db.update(
                "posts",
                values,
                selection,
                selectionArgs);

        Log.e("updatedId",idPost);
    }

    /**
     * Sear
     * @param idPost
     * @return
     */
    public Post searchPost(int idPost) {
        Post response = null;
        SQLiteDatabase db = getReadableDatabase();
        String query="SELECT * FROM posts WHERE id_post="+idPost+" and deleted = 0";
        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst()) {
            response = fillPostModel(c);
        }

        return response;
    }

    public ArrayList<Post> getAllPost() {

        ArrayList<Post> posts = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();
        String query="SELECT * FROM posts WHERE deleted = 0";
        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst()) {
            do {

                posts.add(fillPostModel(c));

            }while(c.moveToNext());
        }

        c.close();

        return posts;
    }

    private Post fillPostModel(Cursor c){
        Post temp = new Post();
        temp.idPost = c.getLong(c.getColumnIndex("id_post"));
        temp.userId = c.getInt(c.getColumnIndex("id_user"));
        temp.bodyPost = c.getString(c.getColumnIndex("body"));
        temp.titlePost = c.getString(c.getColumnIndex("title"));
        temp.favorite = false;
        if(c.getInt(c.getColumnIndex("favorite")) == 1)
            temp.favorite = true;
        temp.newPost = false;
        if(c.getInt(c.getColumnIndex("is_new_post")) == 1)
            temp.newPost = true;


        return temp;
    }

    public void deleteAll() {
        SQLiteDatabase db = getReadableDatabase();
        db.delete("posts", null, null);
    }

    public ArrayList<Post> getAllPostFav() {
        ArrayList<Post> posts = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();
        String query="SELECT * FROM posts WHERE favorite = 1 and deleted = 0";
        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst()) {
            do {

                posts.add(fillPostModel(c));

            }while(c.moveToNext());
        }

        c.close();

        return posts;
    }

    public void deleteId(long idPost){
        SQLiteDatabase db = getReadableDatabase();
        String selection =  "id_post = ?";
        String[] selectionArgs = { idPost+"" };
        db.delete("posts", selection, selectionArgs);
    }

    //---------------------------- users---------------------

    public void manageInsertionUser(ContentValues values){

        if(searchUser(values.getAsInteger("id_user")) != null){
            updateUserInfo(values);
        }else{
            insertUser(values);
        }
    }

    /**
     * Insert values to db
     * @param values
     * @return
     */
    public long insertUser(ContentValues values){
        SQLiteDatabase db = getWritableDatabase();

        long newRowId = db.insert("users", null, values);
        Log.e("InsertedId",""+newRowId);

        return newRowId;
    }

    public void updateUserInfo(ContentValues values){
        SQLiteDatabase db = getReadableDatabase();

        String selection = "id_user = ?";
        String idPost = values.getAsString("id_user");
        String[] selectionArgs = { idPost };

        int count = db.update(
                "users",
                values,
                selection,
                selectionArgs);

        Log.e("updatedUserId",idPost);
    }

    /**
     * Sear
     * @param idPost
     * @return
     */
    public Users searchUser(int idPost) {
        Users response = null;
        SQLiteDatabase db = getReadableDatabase();
        String query="SELECT * FROM users WHERE id_user="+idPost+"";
        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst()) {
            response= new Users();

            response.idUser = c.getLong(c.getColumnIndex("id_user"));
            response.name = c.getString(c.getColumnIndex("name"));
            response.email = c.getString(c.getColumnIndex("email"));
            response.phone = c.getString(c.getColumnIndex("phone"));
            response.website = c.getString(c.getColumnIndex("web"));
        }

        return response;
    }

    //--------------------- Comments -------------------

    public void manageInsertionComments(ContentValues values){

        if(searchComment(values.getAsInteger("id_comment")) != null){
            updateCommentInfo(values);
        }else{
            insertComment(values);
        }
    }

    /**
     * Insert values to db
     * @param values
     * @return
     */
    public long insertComment(ContentValues values){
        SQLiteDatabase db = getWritableDatabase();

        long newRowId = db.insert("coments", null, values);
        Log.e("InsertedIdCommmet",""+newRowId);

        return newRowId;
    }

    public void updateCommentInfo(ContentValues values){
        SQLiteDatabase db = getReadableDatabase();

        String selection = "id_comment = ?";
        String idPost = values.getAsString("id_comment");
        String[] selectionArgs = { idPost };

        int count = db.update(
                "coments",
                values,
                selection,
                selectionArgs);

        Log.e("updatedId",idPost);
    }

    /**
     * Sear
     * @param id_comment
     * @return
     */
    public Comments searchComment(int id_comment) {
        Comments response = null;
        SQLiteDatabase db = getReadableDatabase();
        String query="SELECT * FROM coments WHERE id_comment="+id_comment;
        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst()) {
            response = fillCommentsModel(c);
        }

        return response;
    }

    public ArrayList<Comments> getAllCommentsByPost(long idPOst) {

        ArrayList<Comments> posts = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();
        String query="SELECT * FROM coments WHERE id_post = "+idPOst;
        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst()) {
            do {

                posts.add(fillCommentsModel(c));

            }while(c.moveToNext());
        }

        c.close();

        return posts;
    }

    private Comments fillCommentsModel(Cursor c){
        Comments temp = new Comments();

        temp.idPost = c.getLong(c.getColumnIndex("id_post"));
        temp.body = c.getString(c.getColumnIndex("body"));
        temp.name = c.getString(c.getColumnIndex("name"));
        temp.email = c.getString(c.getColumnIndex("email"));


        return temp;
    }







}
