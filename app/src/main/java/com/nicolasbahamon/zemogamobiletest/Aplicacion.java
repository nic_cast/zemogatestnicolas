package com.nicolasbahamon.zemogamobiletest;

import android.app.Application;
import android.util.TypedValue;

import com.nicolasbahamon.zemogamobiletest.Helpers.DatabaseHelper;
import com.nicolasbahamon.zemogamobiletest.Models.Post;

public class Aplicacion extends Application {


    public DatabaseHelper db;
    public Post postInfo;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public DatabaseHelper getDB(){
        if(db == null){
            db = new DatabaseHelper(getApplicationContext());
        }
        return db;
    }

    public int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                getResources().getDisplayMetrics());
    }
}
